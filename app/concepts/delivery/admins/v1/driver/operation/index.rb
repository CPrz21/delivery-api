module Delivery
  module Admins
    module V1
      class Driver::Operation::Index < Trailblazer::Operation
        step :model!

        def model!(options, params:, **)
          options[:model] = ::Driver.all
        end

      end
    end    
  end
end