module Delivery
  module Admins
    module V1
      class Driver::Operation::Create < Trailblazer::Operation
        step Rescue( handler: :error!){
          step Model(::Driver, :new)
          step Contract::Build(constant: Driver::Contract::Create)
          step Contract::Validate()
          step Contract::Persist()
        }

      end
    end    
  end
end