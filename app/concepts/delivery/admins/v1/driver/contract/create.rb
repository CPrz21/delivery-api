module Delivery
  module Admins
    module V1
      class Driver::Contract::Create < Reform::Form
        property :vehicle
        property :user, populator: :populate_user! do
          property :name
          property :last_name
          property :email
          property :username
          property :password
          property :password_confirmation
          property :status
          property :role

          validation do
            params do
              required(:name).filled
              required(:last_name).filled
              required(:email).filled
              required(:username).filled
              required(:password).filled
              required(:password_confirmation).filled
              required(:status).filled
              required(:role).filled
            end

            rule(:email) do
              user = ::User.find_by(email: value)
              key.failure(:exists) if user
            end
          end
        end

        def populate_user!(model:, **)
          self.user = ::User.new
        end
      end
    end    
  end
end