require 'doorkeeper/grape/helpers'

module Delivery

  module CurrentOwnerHelper
    def current_owner
      return unless doorkeeper_token
    end
  end

  module ResponseOperationHelper
    def response_operation(operation=nil)
      return nil unless operation

      if operation.success?
        operation[:model]
      else
        error!(
          {
            error: operation_error(operation)
          },
          operation['status']
        )
      end
    end
  end

  module OperationErrorHelper
    def operation_error(operation=nil)
      return nil unless operation

      if contract_error?(operation)
        operation['contract.default'].errors.messages
      elsif model_error?(operation)
        operation[:model].errors.messages
      elsif custom_error?(operation)
        operation['error']
      else
        nil
      end
    end

    def contract_error?(operation)
      operation['contract.default'].present? &&
      operation['contract.default'].errors.present? &&
      !operation['contract.default'].errors.blank?
    end

    def model_error?(operation)
      operation[:model].present? &&
      operation[:model].errors.present? &&
      !operation[:model].errors.blank?
    end

    def custom_error?(operation)
      operation['error'].present? &&
      !operation['error'].blank?
    end
  end

  module PaginationParamsHelper
    extend Grape::API::Helpers

    params :pagination do
      optional :page, type: Integer, allow_blank: false, default: 1
      optional :per_page, type: Integer, allow_blank: false, default: 10
    end
  end

  module PaginationHeadersHelper
    extend Grape::API::Helpers

    def set_pagination_headers(operation=nil)
      return unless operation

      if operation.success? & params[:paginate].blank?
        model = operation[:model]

        header 'x-total-pages', model.total_pages
      end
    end
  end

  class API < Grape::API
    format :json
    formatter :json, Grape::Formatter::ActiveModelSerializers
    insert_after Grape::Middleware::Formatter, Grape::Middleware::Logger
    helpers Doorkeeper::Grape::Helpers, CurrentOwnerHelper,
            ResponseOperationHelper, OperationErrorHelper,
            PaginationParamsHelper, PaginationHeadersHelper
    
    # ENDPOINTS
    mount Admins::V1::Root

  end
end