module Delivery
  module Admins
    module V1
      class Root < Base
        version 'v1', using: :path
        prefix :admins
        
        #ENDPOINTS
        mount Drivers
      end
    end    
  end
end