module Delivery
  module Admins
    module V1
      class Base < API
        content_type :json, 'application/json'
        default_format :json
      end
    end    
  end
end