module Delivery
  module Admins
    module V1
      class Drivers < Base
        namespace :drivers do
          desc 'Get Drivers'
          params do
            use :pagination
          end

          get each_serializer: Delivery::Admins::V1::Driver::IndexSerializer do
            
            status :ok

            operation = Delivery::Admins::V1::Driver::Operation::Index.(params: params)

            response_operation(operation)
          end

          desc 'Create Drivers'
          params do
            requires :vehicle, type: String, allow_blank: false, values: ::Driver.vehicles.keys
            requires :user, type: Hash do
              requires :name, type: String, allow_blank: false
              requires :last_name, type: String, allow_blank: false
              requires :email, type: String, allow_blank: false
              requires :username, type: String, allow_blank: false
              requires :password, type: String, allow_blank: false, documentation: { type: 'password' }
              requires :password_confirmation, type: String, same_as: :password, documentation: { type: 'password' }
              requires :status, type: String, allow_blank: false, values: ::User.statuses.keys
              requires :role, type: String, allow_blank: false, values: ::User.roles.keys
            end
          end

          post serializer: Delivery::Admins::V1::Driver::IndexSerializer do
            
            status :created

            operation = Delivery::Admins::V1::Driver::Operation::Create.(params: params)

            response_operation(operation)
          end

        end
      end
    end    
  end
end