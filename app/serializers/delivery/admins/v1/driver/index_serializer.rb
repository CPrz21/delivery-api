module Delivery
  module Admins
    module V1
      module Driver
        class IndexSerializer < ActiveModel::Serializer
          attributes :id, :vehicle, :created_at
          has_one :user
        end
      end
    end    
  end
end