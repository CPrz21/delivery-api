module Delivery
  module Admins
    module V1
      module Driver
        class UserSerializer < ActiveModel::Serializer
          attributes :id, :name, :last_name, :email, :username
        end
      end
    end    
  end
end