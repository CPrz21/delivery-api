class Driver < ApplicationRecord
  has_one :user, as: :userable, dependent: :destroy
  enum vehicle: { carro: 0, moto: 1, bicicleta: 2 }
end
