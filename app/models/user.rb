class User < ApplicationRecord
       belongs_to :userable, polymorphic:true, dependent: :destroy
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :access_grants,
            class_name: 'Doorkeeper::AccessGrant',
            foreign_key: :resource_owner_id,
            dependent: :delete_all # or :destroy if you need callbacks

  has_many :access_tokens,
            class_name: 'Doorkeeper::AccessToken',
            foreign_key: :resource_owner_id,
            dependent: :delete_all # or :destroy if you need callbacks

       # class << self
       enum role: { user: 0, admin: 1, driver: 2 }
       enum status: { activo: 0, inactivo: 1 }
              def self.authenticate!(username, password, scope = 'user')
                     user = User.find_for_authentication(username: username, role: scope)

                     user.need_change_password! if (user && user.change_password?)

                     user.try(:valid_password?, password) ? user : nil
              end
       # end
end
