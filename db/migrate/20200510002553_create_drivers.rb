class CreateDrivers < ActiveRecord::Migration[6.0]
  def change
    create_table :drivers, id: :uuid do |t|
      t.integer :vehicle,       null: false, default: 0 
      t.timestamps
    end
  end
end
